<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <base href="/">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>iTEC 2017</title>

        {{-- Styles --}}
        <link rel="stylesheet" type="text/css" href="{{asset('node_modules/font-awesome/css/font-awesome.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('node_modules/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('node_modules/angular-material/angular-material.min.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        {{-- Custom Styles --}}
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/landing.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/map.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/details.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/header.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/index.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/error.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/statistics.css')}}">
    </head>
    <body ng-app="itecApplication" ng-controller="itecController">
        <ui-view></ui-view>

        {{-- General --}}
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApO1ZpwHg9620yC71J2yEJcaP5LsjdX7k&libraries=places&sensor=false"
                type="text/javascript"></script>
        <script src="{{asset('node_modules/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('node_modules/underscore/underscore-min.js')}}"></script>
        <script src="{{asset('node_modules/lodash/lodash.min.js')}}"></script>
        <script src="{{asset('node_modules/chart.js/dist/Chart.min.js')}}"></script>

        {{-- Angular Modules --}}
        <script src="{{asset('node_modules/angular/angular.min.js')}}"></script>
        <script src="{{asset('node_modules/angular-aria/angular-aria.min.js')}}"></script>
        <script src="{{asset('node_modules/angular-animate/angular-animate.min.js')}}"></script>
        <script src="{{asset('node_modules/angular-material/angular-material.js')}}"></script>
        <script src="{{asset('node_modules/angular-ui-router/release/angular-ui-router.min.js')}}"></script>
        <script src="{{asset('node_modules/satellizer/dist/satellizer.min.js')}}"></script>
        <script src="{{asset('node_modules/angular-simple-logger/dist/angular-simple-logger.min.js')}}"></script>
        <script src="{{asset('node_modules/angular-google-maps/dist/angular-google-maps.min.js')}}"></script>
        <script src="{{asset('node_modules/angular-chart.js/dist/angular-chart.min.js')}}"></script>
        <script src="{{asset('assets/js/angular-dom-events.js')}}"></script>

        {{-- Angular Application Scripts --}}
        <script src="{{asset('app/module.js')}}"></script>
        <script src="{{asset('app/controller.js')}}"></script>
        <script src="{{asset('app/routes.js')}}"></script>
        <script src="{{asset('app/config.js')}}"></script>
        <script src="{{asset('app/components/landing/controller.js')}}"></script>
        <script src="{{asset('app/components/map/controller.js')}}"></script>
        <script src="{{asset('app/components/statistics/controller.js')}}"></script>
        <script src="{{asset('app/components/statistics/services/dayChart.js')}}"></script>
        <script src="{{asset('app/components/statistics/services/raceChart.js')}}"></script>
        <script src="{{asset('app/components/statistics/services/genderChart.js')}}"></script>
        <script src="{{asset('app/components/details/controller.js')}}"></script>
    </body>
</html>