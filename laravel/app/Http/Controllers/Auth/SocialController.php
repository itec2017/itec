<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use Tymon\JWTAuth\Facades\JWTAuth;

class SocialController extends Controller
{
    public function facebook(Request $request)
    {
        $client = new Client();
        $params = [
            'code' => $request->input('code'),
            'client_id' => $request->input('clientId'),
            'redirect_uri' => $request->input('redirectUri'),
            'client_secret' => env('FACEBOOK_SECRET')
        ];

        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->request('GET', 'https://graph.facebook.com/v2.8/oauth/access_token', [
            'query' => $params
        ]);

        $accessToken = json_decode($accessTokenResponse->getBody(), true);

        // Step 2. Retrieve profile information about the current user.
        $fields = 'id, email, first_name, last_name, link, name, gender, picture';
        $profileResponse = $client->request('GET', 'https://graph.facebook.com/v2.8/me', [
            'query' => [
                'access_token' => $accessToken['access_token'],
                'fields' => $fields
            ]
        ]);

        $profile = json_decode($profileResponse->getBody(), true);
        Log::info($profile);

        /* Check if user already exists. */
        $user = User::where('email', '=', $profile['email'])
            ->orWhere('provider', '=', 'facebook')
            ->where('provider_id', '=', $profile['id'])->first();

        if($user) {
            if(! isset($user->provider)) {
                $user->provider = 'facebook';
                $user->provider_id = $profile['id'];
                $user->save();
            }

            return response()->json([
                'token' => JWTAuth::fromUser($user)
            ], 200);
        }

        /* Create new user. */
        $user = new User();
        $user->email = $profile['email'];
        $user->firstname = $profile['first_name'];
        $user->lastname = $profile['last_name'];
        $user->provider = 'facebook';
        $user->provider_id = $profile['id'];
        $user->avatar = 'http://graph.facebook.com/' . $profile['id'] . '/picture?width=1920';
        $user->save();

        return response()->json([
            'token' => JWTAuth::fromUser($user)
        ], 200);
    }
}
