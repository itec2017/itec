app.controller('itecController', function($scope, $http, $state, DatasetService) {
    $scope.stateName = $state.current.name;
    $scope.dataset = [];

    var promise = DatasetService.getData();
    promise.then(function (response) {
        $scope.dataset = response.data;

        angular.forEach($scope.dataset, function(data) {

            if(data.fatal && data.fatal == 'Yes') {
                data.marker_color = 'red';
                return;
            } else if(data.hazmat && data.hazmat == 'Yes') {
                data.marker_color = 'orange';
                return;
            } else if(data.personal_injury && data.personal_injury == 'Yes') {
                data.marker_color = 'yellow';
                return;
            } else if(data.property_damage && data.property_damage == 'Yes') {
                data.marker_color = 'blue';
                return;
            } else {
                data.marker_color = 'green';
            }
        });

        $scope.$broadcast('datasetLoadedEvent', {});
    });
});

app.service("DatasetService", function ($http, $q) {
    this.getData = function(requestURL) {
        var deferred = $q.defer();

        $http({
            headers: {
                'X-App-Token': 'r5LdgqD2VxlV5o05cstIjKSiL'
            },
            method: 'GET',
            url: 'https://data.montgomerycountymd.gov/resource/ms8i-8ux3.json',
        }).then(function successCallback(response, status, headers, config) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.resolve(response);
        });

        return deferred.promise;
    }
});