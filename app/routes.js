app.config(["$locationProvider", function($locationProvider) {
    $locationProvider.html5Mode(true);
}]);

app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/error');

    $stateProvider.state('landing', {
        url: '/',
        templateUrl: 'app/components/landing/view.html',
        controller: 'LandingController'
    }).state('map', {
        url: '/map',
        templateUrl: 'app/components/map/view.html',
        controller: 'MapController'
    }).state('details', {
        url: '/details',
        templateUrl: 'app/components/details/view.html',
        controller: 'DetailsController'
    }).state('auth', {
        url: '/auth',
        templateUrl: 'app/components/auth.html',
        controller: 'LandingController'
    }).state('header', {
        url: '/header',
        templateUrl: 'app/components/shared/header.html'
    }).state('error', {
        url: '/error',
        templateUrl: 'app/components/error/view.html'
    }).state('statistics', {
        url: '/statistics',
        templateUrl: 'app/components/statistics/view.html',
        controller: 'StatisticsController'
    });
});

app.run(['$rootScope', '$state',
    function ($rootScope, $state) {
        $rootScope.$state = $state;
    }
]);