app.controller('StatisticsController', function ($scope, DayChart, RaceChart, GenderChart) {
    console.log('stats...');

    $scope.viewMode = {
        dayChartDailyView: true,
        genderChartDailyView: true
    };

    if($scope.dataset.length) {
        $scope.viewCharts = true;

        $scope.dayChart = DayChart.getConfig($scope.dataset, true);
        $scope.raceChart = RaceChart.getConfig($scope.dataset);
        $scope.genderChart = GenderChart.getConfig($scope.dataset, true);
    } else {
        $scope.$on('datasetLoadedEvent', function () {
            $scope.viewCharts = true;

            $scope.dayChart = DayChart.getConfig($scope.dataset, true);
            $scope.raceChart = RaceChart.getConfig($scope.dataset);
            $scope.genderChart = GenderChart.getConfig($scope.dataset, true);
        });
    }

    $scope.onClick = function (points, evt) {
        console.log(points, evt);
    };

    $scope.setViewDaily = function (viewDaily) {
        $scope.dayChart = DayChart.getConfig($scope.dataset, viewDaily);
    };

    $scope.setDayChartDailyView = function(viewDaily) {
        $scope.dayChart = DayChart.getConfig($scope.dataset, viewDaily);
        $scope.viewMode.dayChartDailyView = viewDaily;
    };

    $scope.setGenderChartDailyView = function(viewDaily) {
        $scope.genderChart = GenderChart.getConfig($scope.dataset, viewDaily);
        $scope.viewMode.genderChartDailyView = viewDaily;
    };
});