app.service("DayChart", function () {
    var dayLabels = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    var monthLabels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    this.getConfig = function(dataset, viewDaily) {
        var chartConfig = {
            labels: viewDaily ? dayLabels : monthLabels,
            series: ['Property Damage', 'Personal Injury', 'Hazardous Materials', 'Fatal'],
            data: [
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0]
            ],
            datasetOverride: [{
                yAxisID: 'y-axis-1'
            }],
            options: {
                scales: {
                    yAxes: [
                        {
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left'
                        }
                    ]
                },
                legend: {
                    display: true
                }
            },
            colors: [{
                backgroundColor: 'rgba(87, 129, 252, 0.5)',
                pointBackgroundColor: 'rgba(87, 129, 252, 1)',
                pointHoverBackgroundColor: 'rgba(87, 129, 252, 1)',
                borderColor: 'rgba(87, 129, 252, 1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(87, 129, 252, 0.8)'
            }, {
                backgroundColor: 'rgba(252, 243, 87, 0.5)',
                pointBackgroundColor: 'rgba(252, 243, 87, 1)',
                pointHoverBackgroundColor: 'rgba(252, 243, 87, 1)',
                borderColor: 'rgba(252, 243, 87, 1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(252, 243, 87, 0.8)'
            }, {
                backgroundColor: 'rgba(255, 153, 0, 0.5)',
                pointBackgroundColor: 'rgba(255, 153, 0, 1)',
                pointHoverBackgroundColor: 'rgba(255, 153, 0, 1)',
                borderColor: 'rgba(255, 153, 0, 1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(255, 153, 0, 0.8)'
            }, {
                backgroundColor: 'rgba(252, 99, 85, 0.5)',
                pointBackgroundColor: 'rgba(252, 99, 85, 1)',
                pointHoverBackgroundColor: 'rgba(252, 99, 85, 1)',
                borderColor: 'rgba(252, 99, 85, 1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(252, 99, 85, 0.8)'
            }]
        };

        angular.forEach(dataset, function (data) {
            if (data.date_of_stop) {
                var index = viewDaily ? (new Date(data.date_of_stop)).getDay() : (new Date(data.date_of_stop)).getMonth();

                if(data.fatal && data.fatal == 'Yes') {
                    chartConfig.data[3][index]++;
                } else if(data.hazmat && data.hazmat == 'Yes') {
                    chartConfig.data[2][index]++;
                } else if(data.personal_injury && data.personal_injury == 'Yes') {
                    chartConfig.data[1][index]++;
                } else if(data.property_damage && data.property_damage == 'Yes') {
                    chartConfig.data[0][index]++;
                }
            }
        });

        return chartConfig;
    }
});