app.service("RaceChart", function () {
    var races = [];

    var isInArray = function(arr, term) {
        for(var i=0; i<arr.length; i++) {
            if(arr[i] == term)
                return true;
        }

        return false;
    };

    this.getConfig = function(dataset) {
        races = [];

        var chartConfig = {
            labels: [],
            data: [],
            colors: [{
                backgroundColor: 'rgba(87,129,252,0.2)',
                pointBackgroundColor: 'rgba(87,129,252,1)',
                pointHoverBackgroundColor: 'rgba(87,129,252,1)',
                borderColor: 'rgba(87,129,252,1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(87,129,252,0.8)'
            }, {
                backgroundColor: 'rgba(0,225,60,0.2)',
                pointBackgroundColor: 'rgba(0,225,60,1)',
                pointHoverBackgroundColor: 'rgba(0,225,60,1)',
                borderColor: 'rgba(0,225,60,1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(0,225,60,0.8)'
            }, {
                backgroundColor: 'rgba(252,243,87,0.2)',
                pointBackgroundColor: 'rgba(252,243,87,1)',
                pointHoverBackgroundColor: 'rgba(252,243,87,1)',
                borderColor: 'rgba(252,243,87,1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(252,243,87,0.8)'
            }, {
                backgroundColor: 'rgba(255,153,0,0.2)',
                pointBackgroundColor: 'rgba(255,153,0,1)',
                pointHoverBackgroundColor: 'rgba(255,153,0,1)',
                borderColor: 'rgba(255,153,0,1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(255,153,0,0.8)'
            }, {
                backgroundColor: 'rgba(252,99,85,0.2)',
                pointBackgroundColor: 'rgba(252,99,85,1)',
                pointHoverBackgroundColor: 'rgba(252,99,85,1)',
                borderColor: 'rgba(252,99,85,1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(252,99,85,0.8)'
            }],
            options: {
                legend: {
                    display: true
                }
            }
        };

        angular.forEach(dataset, function(data) {
           if(! isInArray(races, data.race.toLowerCase())) {
               races.push(data.race.toLowerCase());

               chartConfig.labels.push(data.race.substring(0, 1) + data.race.substring(1).toLowerCase());
               chartConfig.data.push(0);
           }

            chartConfig.data[races.indexOf(data.race.toLowerCase())]++;
        });

        console.log(races);
        console.log(chartConfig.data);

        return chartConfig;
    }
});