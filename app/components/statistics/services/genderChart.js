app.service("GenderChart", function () {
    var dayLabels = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    var monthLabels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    this.getConfig = function(dataset, viewDaily) {
        var chartConfig = {
            labels: viewDaily ? dayLabels : monthLabels,
            series: ['Male', 'Female'],
            data: [
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0]
            ],
            datasetOverride: [{
                yAxisID: 'y-axis-1'
            }],
            options: {
                scales: {
                    yAxes: [
                        {
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left'
                        }
                    ]
                },
                legend: {
                    display: true
                }
            },
            colors: [{
                backgroundColor: 'rgba(173,216,230,0.2)',
                pointBackgroundColor: 'rgba(173,216,230,1)',
                pointHoverBackgroundColor: 'rgba(173,216,230,1)',
                borderColor: 'rgba(173,216,230,1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(173,216,230,0.8)'
            }, {
                backgroundColor: 'rgba(255,182,193,0.2)',
                pointBackgroundColor: 'rgba(255,182,193,1)',
                pointHoverBackgroundColor: 'rgba(255,182,193,1)',
                borderColor: 'rgba(255,182,193,1)',
                pointBorderColor: '#fff',
                pointHoverBorderColor: 'rgba(255,182,193,0.8)'
            }]
        };

        angular.forEach(dataset, function (data) {
            if (data.date_of_stop) {
                var index = viewDaily ? (new Date(data.date_of_stop)).getDay() : (new Date(data.date_of_stop)).getMonth();

                if(data.gender) {
                    if(data.gender == 'M') {
                        chartConfig.data[0][index]++;
                    } else {
                        chartConfig.data[1][index]++;
                    }
                }
            }
        });

        return chartConfig;
    }
});