app.controller('MapController', function ($scope) {
    $scope.filters = {
        others: true,
        property_damage: true,
        personal_injury: true,
        hazmat: true,
        fatal: true
    };

    $scope.map = {
        center: {
            latitude: 39.068688,
            longitude: -77.11421
        },
        zoom: 14
    };

    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
        ].join('/');
    };

    $scope.setViolation = function(data) {
        localStorage.setItem('violation', JSON.stringify(data));

        $('#violation-details-placeholder').hide();
        $('#violation-details').show();

        $('#violation-location').html(data.location);
        $('#violation-date').html(new Date(data.date_of_stop).yyyymmdd());
        $('#violation-description').html(data.description);
    };

    $scope.configElements = function() {
        $('.datepicker').datepicker({
            format: "yyyy-mm",
            startView: "years",
            minViewMode: "months",
            todayHighlight: true,
            autoclose: true
        });
    };
});