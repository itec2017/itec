app.controller('LandingController', function($scope, $auth, Account) {
    $scope.authenticate = function (provider) {

        $auth.authenticate(provider)
            .then(function (response) {
                $auth.setToken(response.data.token);
            })
            .catch(function (error) {
                if (error.message) {
                    console.log(error.message);
                } else if (error.data) {
                    console.log(error.data.message, error.status);
                } else {
                    console.log(error);
                }
            });
    };

    $scope.login = function (credentials) {
        $auth.login(credentials).then(function (data) {
            console.log(data);
        });
    };

    $scope.getProfile = function() {
        Account.getProfile()
            .then(function(response) {
                console.log(response);
                $scope.user = response.data;
            })
            .catch(function(response) {
                console.log(response);
            });
    };
});