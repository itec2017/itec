app.controller('DetailsController', function($scope, $state) {
    $scope.violation = localStorage.getItem('violation');

    if(!$scope.violation) {
        $state.go('map');
    }

    $scope.violation = JSON.parse($scope.violation);

    console.log($scope.violation);

    $scope.map = {
        center: {
            latitude: $scope.violation.geolocation.coordinates[1],
            longitude: $scope.violation.geolocation.coordinates[0]
        },
        zoom: 14
    };
});