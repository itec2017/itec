app.config(function($authProvider) {
    $authProvider.loginUrl = '/itec/api/authenticate';

    $authProvider.facebook({
        clientId: '1795864874006179',
        url: '/api/auth/facebook'
    });
});

app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);