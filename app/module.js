var app = angular.module('itecApplication', [
    'ngAria',
    'ngAnimate',
    'ngMaterial',
    'ui.router',
    'satellizer',
    'nemLogging',
    'uiGmapgoogle-maps',
    'chart.js',
    'ngDomEvents',
]);

app.factory('Account', function($http, $auth) {
    return {
        getProfile: function() {
            return $http.get('/api/me');
        },
        updateProfile: function(profileData) {
            return $http.put('/api/me', profileData);
        }
    };
});

app.filter('geolocationFilter', function() {
    return function(dataset, filters, startDateString, endDateString) {
        var response = [];

        angular.forEach(dataset, function(value) {
            if(value.geolocation) {
                if(startDateString && endDateString) {
                    var startDate = new Date(startDateString + '-01');
                    var endDate = new Date(endDateString + '-28');
                    var date = new Date(value.date_of_stop);

                    if(startDate <= date && date <= endDate) {
                        if(filters.fatal && value.fatal && value.fatal == 'Yes') {
                            response.push(value);
                        } else if(filters.hazmat && value.hazmat && value.hazmat == 'Yes') {
                            response.push(value);
                        } else if(filters.personal_injury && value.personal_injury && value.personal_injury == 'Yes') {
                            response.push(value);
                        } else if(filters.property_damage && value.property_damage && value.property_damage == 'Yes') {
                            response.push(value);
                        } else if(filters.others) {
                            if((!value.fatal || value.fatal == 'No') && (!value.hazmat || value.hazmat == 'No') &&
                                (!value.personal_injury || value.personal_injury == 'No') &&
                                (!value.property_damage || value.property_damage == 'No'))
                                response.push(value);
                        }
                    }
                } else {
                    if(filters.fatal && value.fatal && value.fatal == 'Yes') {
                        response.push(value);
                    } else if(filters.hazmat && value.hazmat && value.hazmat == 'Yes') {
                        response.push(value);
                    } else if(filters.personal_injury && value.personal_injury && value.personal_injury == 'Yes') {
                        response.push(value);
                    } else if(filters.property_damage && value.property_damage && value.property_damage == 'Yes') {
                        response.push(value);
                    } else if(filters.others) {
                        if((!value.fatal || value.fatal == 'No') && (!value.hazmat || value.hazmat == 'No') &&
                            (!value.personal_injury || value.personal_injury == 'No') &&
                            (!value.property_damage || value.property_damage == 'No'))
                            response.push(value);
                    }
                }
            }
        });

        return response;
    }
});