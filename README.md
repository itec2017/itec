#Install instructions of iTEC2017 Project.

Git clone ```https://bitbucket.org/itec2017/itec.git```


Install PHP Composer: https://getcomposer.org/doc/00-intro.md#installation-windows


Change directory to <itec/laravel>

Run in terminal/cmd ```composer install```

Copy ```.env.example``` file and rename it as ```.env```.

Run in terminal/cmd ```php artisan key:generate```

Change directory to <itec>

Run in terminal/cmd ```npm install```

Open ```..\WINDOWS\System32\drivers\etc\hosts``` file and edit it as an administrator.

Add the following line at the end of the file ```127.0.0.1	itec.dev```

Open ```..\xampp\apache\conf\extra\httpd-vhosts.conf``` and edit it as an administrator.

Add the following line at the end of the file ```<VirtualHost *:80>
    DocumentRoot "../xampp/htdocs/itec"
    ServerName itec.dev
</VirtualHost>```

Restart the XAMPP if needed.

Navigate to ```itec.dev```

**If you use others programs all you have to do is to create a virtual machine on your computer that points to the itec folder.**

**Thank you! Enjoy!**